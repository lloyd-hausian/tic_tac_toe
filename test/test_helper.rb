$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'tic_tac_toe'

require 'minitest'
require 'minitest/autorun'
require 'minitest/reporters'
require 'pry'

Minitest::Reporters.use! [Minitest::Reporters::DefaultReporter.new(color: true)]
