require 'ostruct'
require 'minitest/autorun'
require 'tic_tac_toe/board'
require 'tic_tac_toe/exceptions'

describe TicTacToe::Board do
  let(:point) { OpenStruct.new(x: 0, y: 0) }
  let(:invalid_point_under) { OpenStruct.new(x: -1, y: -1) }
  let(:invalid_point_over) { OpenStruct.new(x: -1, y: -1) }
  let(:winning_point) { OpenStruct.new(x: 2, y: 2) }
  let(:marker) { :X }
  let(:board) { TicTacToe::Board.new }
  let(:excepted) { [[:X,:_, :_],[:_,:_,:_],[:_,:_,:_]] }
  let(:almost_won_horizontal) {
    TicTacToe::Board.new(board: [[:_,:_, :_],[:_,:_,:_],[:X,:X,:_]])
  }
  let(:almost_won_vertical) {
    TicTacToe::Board.new(board: [[:_,:_, :X],[:_,:_,:X],[:X,:_,:_]])
  }
  let(:almost_won_diagonally) {
    TicTacToe::Board.new(board: [[:X,:_, :X],[:_,:X,:_],[:_,:_,:_]])
  }

  describe '#place' do
    it 'places marker in correct position' do
      board.place(point.x, point.y, marker)

      assert_equal excepted, board.field
    end

    describe '#validate_point' do
      it 'fails for invalid points under zero' do
        assert_raises TicTacToe::OutOfBounds do
          board.place(invalid_point_under.x, invalid_point_under.y, marker)
        end
      end

      it 'fails for invalid points for the limit' do
        assert_raises TicTacToe::OutOfBounds do
          board.place(invalid_point_over.x, invalid_point_over.y, marker)
        end
      end
    end

    describe '#game_over?' do
      it 'returns true when there is 3 in a row horizontally' do
        refute almost_won_horizontal.game_over?
        almost_won_horizontal.place(winning_point.x, winning_point.y, marker)
        assert almost_won_horizontal.game_over?
      end

      it 'returns true when there is 3 in a row vertically' do
        refute almost_won_vertical.game_over?
        almost_won_vertical.place(winning_point.x, winning_point.y, marker)
        assert almost_won_vertical.game_over?
      end

      it 'returns true when there is 3 in a row diagonally' do
        refute almost_won_diagonally.game_over?
        almost_won_diagonally.place(winning_point.x, winning_point.y, marker)
        assert almost_won_diagonally.game_over?
      end
    end
  end
end
