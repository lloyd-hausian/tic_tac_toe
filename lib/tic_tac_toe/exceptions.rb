module TicTacToe
  class TicTacToeException < Exception
  end

  class GameOver < TicTacToeException
  end

  class OutOfBounds < TicTacToeException
  end
end
