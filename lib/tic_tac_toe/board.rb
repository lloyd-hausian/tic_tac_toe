require 'tic_tac_toe/exceptions'

module TicTacToe
  class Board
    attr_reader :limit, :last_move
    attr_accessor :field
    def initialize(board: nil)
      @field =  board || [[:_]*3,[:_]*3, [:_]*3]
      @limit = 2
      @last_move = nil
    end

    def place(x, y, marker)
      validate_point(x, y)
      @last_move = {x: x, y: y, marker: marker}

      field[y][x] = marker

      game_over?
    end

    def game_over?
      horizontal_win? || vertical_win? || diagonal_win?
    end

    def horizontal_win?
      return false unless last_move

      point = @last_move[:y]
      field[point].all? do |entry|
        entry == @last_move[:marker]
      end
    end

    def vertical_win?
      return false unless last_move

      point = @last_move[:x]
      field.all? do |entry|
        entry[point] == @last_move[:marker]
      end
    end

    def diagonal_win?
      return false unless last_move

      (0..limit).map{|i| field[i][i]}.all?{|x| x == @last_move[:marker] }
    end
    def validate_point(x, y)
      fail OutOfBounds if x < 0 || x > limit
      fail OutOfBounds if y < 0 || y > limit
    end
  end
end
